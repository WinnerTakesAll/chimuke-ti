package jp.co.team_assignment.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.team_assignment.entity.User;

@WebFilter(urlPatterns= { "/*" })
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String path = ((HttpServletRequest)request).getServletPath();
		/*System.out.println(path);*/
		if((!("/login/".equals(path))) && !((path).matches(".*.css")) && !((path).matches(".*.js"))){


			HttpSession session = ((HttpServletRequest)request).getSession();

			if(session == null/* && !("/profile/{id}/".equals(path)) || !("/update/{id}/".equals(path)*/){
				((HttpServletResponse)response).sendRedirect("../login/");
			/* else { ((HttpServletResponse)response).sendRedirect("../login/");*/
			}

			User user = (User) session.getAttribute("loginUser");

			if (user == null) {
				((HttpServletResponse)response).sendRedirect("../login/");
			}else{
				chain.doFilter(request, response); // サーブレットを実行
			}
		}else{
			chain.doFilter(request, response);
		}
	}

	public void init(FilterConfig config) {
	}

	public void destroy() {
	}
}
