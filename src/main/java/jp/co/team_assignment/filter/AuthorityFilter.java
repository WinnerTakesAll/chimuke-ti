package jp.co.team_assignment.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.team_assignment.entity.User;
import jp.co.team_assignment.mapper.UserMapper;
import jp.co.team_assignment.service.UserService;

@WebFilter(urlPatterns= { "/signUp/" })
	public class AuthorityFilter implements Filter {

	@Autowired UserService userService;
	@Autowired UserMapper userMapper;

		public void doFilter(ServletRequest request, ServletResponse response,
				FilterChain chain) throws IOException, ServletException {

			HttpSession session = ((HttpServletRequest) request).getSession();

/*			if(session == null){
				chain.doFilter(request, response);
				return;*/

			User user = (User) session.getAttribute("loginUser");
			if(user == null){
				chain.doFilter(request, response);
				return;
			}
			//String path = ((HttpServletRequest)request).getServletPath();
			/*	HttpSession session = ((HttpServletRequest)request).getSession();*/
			User loginUser = (User) session.getAttribute("loginUser");
			if (loginUser.getAdmin_flg() != 1) {
				((HttpServletResponse)response).sendRedirect("../profile/");
				return;
			}
			chain.doFilter(request, response);
		}

		public void init(FilterConfig config) {
		}
		public void destroy() {
		}
}

/*@WebFilter(urlPatterns= { "/signUp/" })
public class AuthorityFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		if(session == null){
			chain.doFilter(request, response);
			return;
		}
		User user = (User) session.getAttribute("loginUser");
		if(user == null){
			chain.doFilter(request, response);
			return;
		}

		if (user.position_id != 3 || user.position_id != 4 || user.position_id != 6 ) {
			((HttpServletResponse)response).sendRedirect("../profile/");
			return;
		}
		chain.doFilter(request, response);
	}
	public void init(FilterConfig config) {
	}
	public void destroy() {
	}*/

