package jp.co.team_assignment.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.team_assignment.dto.SearchDto;
import jp.co.team_assignment.form.SearchForm;
import jp.co.team_assignment.service.SearchService;

@Controller
public class SearchController {

	@Autowired
	HttpSession session;

	@Autowired
	private SearchService searchService;

	@RequestMapping(value = "/search/", method = RequestMethod.GET)
	public String search(Model model) {
/*		int loginId = ((User)session.getAttribute("loginUser"));*/
		SearchForm form = new SearchForm();
		model.addAttribute("SearchForm", form);
		model.addAttribute("message", "検索してもええで");
		return "search";
	}

	@RequestMapping(value = "/search/", params = "searchTags", method = RequestMethod.GET)
	public String searchHashtag(@ModelAttribute("SearchForm") SearchForm form, Model model) {
/*		System.out.println(form.getHashtag());*/
		List<SearchDto> hashtagList = searchService.searchHashtags(form);
		model.addAttribute("SearchForm", form);
		model.addAttribute("searchHashtags", hashtagList);
/*		System.out.println(hashtagList.size());*/
	    model.addAttribute("message", "検索結果やで");
		return "search";
	}

	@RequestMapping(value = "/search/", params = "searchTexts", method = RequestMethod.GET)
	public String searchText(@ModelAttribute("SearchForm") SearchForm form, Model model) {
		List<SearchDto> textList = searchService.searchTexts(form);
/*//		System.out.println(textList.size());
//		System.out.println(form);
*/		model.addAttribute("SearchForm", form);
		model.addAttribute("searchTexts", textList);
	    model.addAttribute("message", "検索結果やで");
		return "search";
	}


	@RequestMapping(value = "/search/", params = "searchUsers", method = RequestMethod.GET)
	public String searchUser(@ModelAttribute("SearchForm") SearchForm form, Model model) {
		/*SearchForm form = new SearchForm();*/
/*		System.out.println(form.getUser_name());*/
		List<SearchDto> userList = searchService.searchUsers(form);
/*		System.out.println(userList.size());*/
		model.addAttribute("SearchForm", form);
		model.addAttribute("searchUsers", userList);
	    model.addAttribute("message", "検索結果やで");

		return "search";
	}

}
