package jp.co.team_assignment.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.team_assignment.dto.MessageDto;
import jp.co.team_assignment.dto.PersonalityDto;
import jp.co.team_assignment.dto.PositionDto;
import jp.co.team_assignment.dto.UserDto;
import jp.co.team_assignment.entity.Message;
import jp.co.team_assignment.entity.Personality;
import jp.co.team_assignment.entity.User;
import jp.co.team_assignment.form.EditForm;
import jp.co.team_assignment.form.MessageForm;
import jp.co.team_assignment.form.SearchForm;
import jp.co.team_assignment.form.SignUpForm;
import jp.co.team_assignment.service.MessageService;
import jp.co.team_assignment.service.UserService;

@Controller
public class UserController {

	@Autowired
	HttpSession session;
	@Autowired
	private MessageService messageService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/signUp/", method = RequestMethod.GET)
	public String signUp(Model model) {
	    SignUpForm form = new SignUpForm();
	    model.addAttribute("SignUpForm", form);
	    model.addAttribute("message", "ユーザー登録してな");

	    List<PositionDto> positionList = userService.getPositions();
/*		positionList.add(new PositionForm(1,"JSA"));
		positionList.add(new PositionForm(2,"技術部"));
		positionList.add(new PositionForm(3,"ROU"));
		positionList.add(new PositionForm(4,"WBU"));
		positionList.add(new PositionForm(5,"管理部"));
		positionList.add(new PositionForm(6,"VOE"));*/
		model.addAttribute("positionList",positionList);

		List<PersonalityDto> personalityList = userService.getPersonalities();
/*		personalityList.add(new PersonalityForm(1,"前向き"));
		personalityList.add(new PersonalityForm(2,"慎重"));
		personalityList.add(new PersonalityForm(3,"楽観主義"));
		personalityList.add(new PersonalityForm(4,"悲観主義"));
		personalityList.add(new PersonalityForm(5,"合理主義"));*/
		model.addAttribute("checkPersonalities", personalityList);

	    return "signUp";
	}

	@RequestMapping(value = "/signUp/", method = RequestMethod.POST)
	public String signUp(@Valid @ModelAttribute("SignUpForm") SignUpForm form, BindingResult result, Model model) {

//		model.addAttribute("SignUpForm", form);
		if(result.hasErrors()) {
			//エラー
			/*model.addAttribute("title", "エラー");*/
			model.addAttribute("message", "もう一回入力してな");
			List<PersonalityDto> personalityList = userService.getPersonalities();
			model.addAttribute("checkPersonalities", personalityList);
			List<PositionDto> positionList = userService.getPositions();
			model.addAttribute("positionList",positionList);
		} else {
			//成功
			int user_id  = userService.signUp(form);
			int[] selectedPersonalities = form.getPersonality_id();
			/*System.out.println(selectedPersonalities.length);*/
			for(int personality_id : selectedPersonalities ){
				userService.userPersonalityRegister(user_id , personality_id);
			}


			List<PersonalityDto> personalityList = userService.getPersonalities();
			model.addAttribute("checkPersonalities", personalityList);
			List<PositionDto> positionList = userService.getPositions();
			model.addAttribute("positionList",positionList);
/*		    Logger.getLogger(UserController.class).log(Level.INFO, "登録件数は" + count + "件です。");*/
		return "redirect:/profile/";
		}
		return "signUp";
	}

	//編集画面
	@RequestMapping(value = "/update/{id}/", method = RequestMethod.GET)
	public String update(Model model, @PathVariable int id) {
		UserDto loginUser = userService.getLoginUser(id);
/*		((User) session.getAttribute("loginUser")).getId();*/
	    model.addAttribute("message", "更新してもええで");
	    model.addAttribute("loginUser", loginUser);
	    EditForm form = new EditForm();
	    form.setId(loginUser.getId());
	    form.setName(loginUser.getName());
	    form.setMail_address(loginUser.getMail_address());
	    form.setPosition_id(loginUser.getPosition_id());
	    form.setJoined_date(loginUser.getJoined_date());
	    form.setPersonality_id(loginUser.getPersonality_id());
	    form.setCareer_id(loginUser.getCareer_id());
	    form.setIcon(loginUser.getIcon());
	    form.setProfile_text(loginUser.getProfile_text());
		List<PersonalityDto> personalityList = userService.getPersonalities();
		model.addAttribute("checkPersonalities", personalityList);
		List<PositionDto> positionList = userService.getPositions();
		model.addAttribute("positionList",positionList);
	    model.addAttribute("EditForm", form);
	    User user = (User) session.getAttribute("loginUser");
	    System.out.println(user.admin_flg);
	    model.addAttribute("loginUser",user);
	    return "update";
	}

	@RequestMapping(value = "/profile/{id}/", method = RequestMethod.GET)
	public String othersProfile(Model model, @PathVariable int id) {
		UserDto otherUser = userService.getOthers(id);
		SearchForm form = new SearchForm();

/*		textList.add(userService.getOthers(id));*/

		/*form.setId(otherUser.getId());*/
		form.setProfile_text(otherUser.getProfile_text());
		form.setName(otherUser.getName());
		form.setJoined_date_disp(otherUser.getJoined_date_disp());
		form.setPosition_name(otherUser.getPosition_name());
		form.setCareer_id(otherUser.getCareer_id());
		form.setText(otherUser.getText());
		/*form.setText(otherUser.getText());*/
		/*form.setHashtag_name(otherUser.getHashtag_name());*/
		/*System.out.println(form.getHashtag());*/
//			System.out.println(form.getHashtag_name());
		/*int othersId =  ((User) .getAttribute("otherUser")).getId();*/
		if(otherUser.getText() == null) {
			model.addAttribute("message", "まだ投稿されてへんわ");
		}

		Message othersMessage = new Message();
		othersMessage.setUser_id(id);
		List<MessageDto> textList = messageService.getAlltexts(othersMessage);
			model.addAttribute("SearchForm", form);
			model.addAttribute("otherUsers", otherUser);
			model.addAttribute("textLists", textList);

			return "otherProfile";
	}

	//更新
	@RequestMapping(value = "/update/{id}/", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute("EditForm") EditForm form, BindingResult result, Model model) {
		if(result.hasErrors()) {
			model.addAttribute("message", "もう一回入力してな");
			List<PersonalityDto> personalityList = userService.getPersonalities();
			model.addAttribute("checkPersonalities", personalityList);
			List<PositionDto> positionList = userService.getPositions();
			model.addAttribute("positionList",positionList);
		} else {
			//成功
			UserDto loginUser = new UserDto();
		    BeanUtils.copyProperties(form, loginUser);
			List<PersonalityDto> personalityList = userService.getPersonalities();
			model.addAttribute("checkPersonalities", personalityList);
			List<PositionDto> positionList = userService.getPositions();
			model.addAttribute("positionList",positionList);
		    userService.update(loginUser);
		    return "redirect:/profile/";
		}
		return "update";
	}


//ログイン画面
	@RequestMapping(value = "/login/", method = RequestMethod.GET)
	public String login(Model model){
		User loginForm = new User();
		model.addAttribute("userForm",loginForm);
		model.addAttribute("message","Sign In!");
		return "login";
	}

//ログイン→プロフィール
	@RequestMapping(value = "/login/", method = RequestMethod.POST)
	public String login(@ModelAttribute("loginForm") User loginForm, Personality personality, Model model){

		/*UserDto dto = new UserDto();*/
/*		UserDto userList = UserService.getUsers(password, mail_address);
		if(userList != null == true) {
			model.addAttribute("messages","");

		} else {*/
/*		List<Personality> name = personality.getPersonalities_name();*/
		User loginUser = userService.login(loginForm);
/*		List<PersonalityDto> userPersonality = userService.getUserPersonalities(loginForm);*/
		session.setAttribute("loginUser", loginUser);
	/*	session.setAttribute("loginUser", userPersonality);*/
		session.setAttribute("profileMessage","プロフィールやで");
		MessageForm form = new MessageForm();
		model.addAttribute("messageForm",form);
		return "redirect:/profile/";
	}
/*	}
		return "login";
	}*/

//プロフィール画面

	@RequestMapping(value = "/profile/", method = RequestMethod.GET)
	public String profile(User userForm, Model model, HttpServletResponse response) throws IOException, ServletException {
		Message messageForm = new Message();
		model.addAttribute("messageForm" , messageForm);
		model.addAttribute("message" , "投稿してね");
		int loginId = ((User) session.getAttribute("loginUser")).getId();

/*		response.setContentType("image/png");

		byte[] icon = ((User)session.getAttribute("loginUser")).getIcon();
		ServletOutputStream outputStream = response.getOutputStream();
		ByteArrayInputStream bais = new ByteArrayInputStream(icon);
		StreamUtil.copy(bais, outputStream);
		outputStream.flush();
*/

			messageForm.setUser_id(loginId);
			List<MessageDto> textList = messageService.getAlltexts(messageForm);
			model.addAttribute("textLists", textList);

/*			if(textLis == null) {
			model.addAttribute("textLists", "まだ投稿がありません");
			return "profile";
		}*/
			return "profile";
	}
}
/*		@ResponseBody
		@RequestMapping("/profile")
		public ResponseEntity<byte[]> icon(HttpServletResponse response, @PathVariable("icon") int id) throws IOException {
				Image img = userService.getUserIcon();

				return new ResponseEntity<byte[]>(img.getImgData(), HttpStatus.CREATED);
	}
}*/



