package jp.co.team_assignment.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.team_assignment.entity.Message;
import jp.co.team_assignment.service.MessageService;

@Controller
public class MessageController {

	@Autowired
	HttpSession session;
	@Autowired
	HttpServletRequest request;
	@Autowired
	private MessageService messageService;
/*	@Autowired Message messageForm;*/
/*	private Message messageForm = new Message();*/

	@RequestMapping(value="/profile/", params="publish" , method = RequestMethod.POST)
	public String publish(@Valid @ModelAttribute("messageForm") Message messageForm, Model model){
		List<String> messageList = new ArrayList<String>();
		if(isValid(messageForm, messageList) == false) {
	/*		model.addAttribute("errorMessages", messageList);*/
		} else {
//			String publishText = messageForm.getText();
			int publish_id = messageService.textRegister(messageForm);

			String hashtags = messageForm.getHashtag_name();
			String[] publishHashtagList = hashtags.split("[,\\s　、]+");
/*			System.out.println(publishHashtagList.length);*/

			for (String hashtag_name : publishHashtagList){
//				hashtag_name = messageForm.getHashtag_name();

				int hashtag_check = messageService.hashtagCheck(hashtag_name);
				if ( hashtag_check == 0 ){
					messageService.hashtagRegister(hashtag_name);
				}

				//ハッシュタグと投稿の｢テーブルをひも付けてひとまとめにする
				int hashtag_id = messageService.selectHashtag(hashtag_name);
				messageService.publishHashtagRegister(publish_id , hashtag_id);
			}
			return "redirect:/profile/";
		}
		return "redirect:/profile/";
	}

	private boolean isValid(Message messageForm, List<String> messageList) {
		/*String hashtag = messageForm.getHashtag_name();*/

		String text = messageForm.getText();
		String hashtag = messageForm.getHashtags();


		 if (StringUtils.isBlank(text) == true) {
			 messageList.add("本文を入力してね");
		 } else if ( 400 <= text.length()) {
			 messageList.add("本文は400文字以下で入力してね");
		 }
		 if(!StringUtils.isBlank(hashtag) && 150 <= hashtag.length()) {
			 messageList.add("ハッシュタグは150文字以下で入力してね");
		 }
		 if(messageList.size() == 0) {
		      return true;
		 } else {
		     return false;
		 }
	}


	@RequestMapping(value="/profile/", params="deleteMessage" , method = RequestMethod.POST)
	public String deleteMessage(@ModelAttribute("messageForm") Message messageForm , Model model){
		messageService.deleteMessage(messageForm.getPublish_id());
        return "redirect:/profile/";
    }
}
