package jp.co.team_assignment.dto;

import java.util.Date;

public class SearchDto {
	private int id;
	private String hashtag;
	private String hashtag_name;
	private String text;
	private String profile_text;
	private String publish;
	private String user_name;
	private String name;
	private String firstDate;
	private String finalDate;
	private String searchUsers;
	private String searchTexts;
	private String searchHashtags;
	private Date created_date;


	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getProfile_text() {
		return profile_text;
	}
	public void setProfile_text(String profile_text) {
		this.profile_text = profile_text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHashtag_name() {
		return hashtag_name;
	}
	public void setHashtag_name(String hashtag_name) {
		this.hashtag_name = hashtag_name;
	}
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}
	public String getSearchTexts() {
		return searchTexts;
	}
	public void setSearchTexts(String searchTexts) {
		this.searchTexts = searchTexts;
	}
	public String getSearchHashtags() {
		return searchHashtags;
	}
	public void setSearchHashtags(String searchHashtags) {
		this.searchHashtags = searchHashtags;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getFirstDate() {
		return firstDate;
	}
	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getSearchUsers() {
		return searchUsers;
	}
	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}

}
