package jp.co.team_assignment.dto;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.co.team_assignment.entity.User;


@Entity
@Table(name = "users")
public class UserDto {
	private Integer id;
	@Id
	@Column(name = "mail_address")
	private String mail_address;
	private int[] personality_id;
	@Column(name = "password")
	private String password;
    private String name;
    private List<User> personalities_name;
    private String personality_name;
    private String user_name;
	private int position_id;
	private int hashtag_id;
	private String color_code;
	private String icon;
	private String text;
	private String position_name;
	private int career_id;
	private String joined_date;
	private String hashtag;
	private String hashtag_name;
	private String profile_text;



	public String getPersonality_name() {
		return personality_name;
	}
	public void setPersonality_name(String personality_name) {
		this.personality_name = personality_name;
	}
	public List<User> getPersonalities_name() {
		return personalities_name;
	}
	public void setPersonalities_name(List<User> personalities_name) {
		this.personalities_name = personalities_name;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public String getHashtag_name() {
		return hashtag_name;
	}
	public void setHashtag_name(String hashtag_name) {
		this.hashtag_name = hashtag_name;
	}
	public int getAdmin_flg() {
		return admin_flg;
	}
	public void setAdmin_flg(int admin_flg) {
		this.admin_flg = admin_flg;
	}
	private Timestamp created_date;
	private Timestamp updated_date;
	public  int admin_flg;



	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMail_address() {
		return mail_address;
	}
	public void setMail_address(String mail_address) {
		this.mail_address = mail_address;
	}
	public int[] getPersonality_id() {
		return personality_id;
	}
	public void setPersonality_id(int[] personality_id) {
		this.personality_id = personality_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * user_nameを取得します。
	 * @return user_name
	 */
	public String getUser_name() {
	    return user_name;
	}
	/**
	 * user_nameを設定します。
	 * @param user_name user_name
	 */
	public void setUser_name(String user_name) {
	    this.user_name = user_name;
	}
	public int getPosition_id() {
		return position_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}
	public int getHashtag_id() {
		return hashtag_id;
	}
	public void setHashtag_id(int hashtag_id) {
		this.hashtag_id = hashtag_id;
	}
	public String getColor_code() {
		return color_code;
	}
	public void setColor_code(String color_code) {
		this.color_code = color_code;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getCareer_id() {
		return career_id;
	}
	public void setCareer_id(int career_id) {
		this.career_id = career_id;
	}
	public String getJoined_date() {
		return joined_date;
	}
	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}
	public String getJoined_date_disp() {
		String month = this.joined_date.substring(4);
		String ret = this.joined_date.substring(0, 4) + "年" + month;
		return ret;
	}
	public String getProfile_text() {
		return profile_text;
	}
	public void setProfile_text(String profile_text) {
		this.profile_text = profile_text;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
}
