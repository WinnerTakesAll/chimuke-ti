package jp.co.team_assignment.dto;

import java.sql.Timestamp;

public class MessageDto {
	private Integer id;
	private String text;
	private Timestamp created_date;
	private Timestamp updated_date;
	private Integer user_id;
    private String user_name;
    private String hashtag_name;
    private int publish_id;
    private String firstDate;
    private String finalDate;
    private String firstDateTime;
    private String finalDateTime;


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
	public String getFirstDateTime() {
		return firstDate + " 00:00:00";
	}
	public void setFirstDateTime(String firstDateTime) {
		this.firstDateTime = firstDateTime;
	}
	public String getFinalDateTime() {
		return finalDate + " 23:59:59";
	}
	public void setFinalDateTime(String finalDateTime) {
		this.finalDateTime = finalDateTime;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	/**
	 * hashtag_nameを取得します。
	 * @return hashtag_name
	 */
	public String getHashtag_name() {
	    return hashtag_name;
	}
	/**
	 * hashtag_nameを設定します。
	 * @param hashtag_name hashtag_name
	 */
	public void setHashtag_name(String hashtag_name) {
	    this.hashtag_name = hashtag_name;
	}
	/**
	 * publish_idを取得します。
	 * @return publish_id
	 */
	public int getPublish_id() {
	    return publish_id;
	}
	/**
	 * publish_idを設定します。
	 * @param publish_id publish_id
	 */
	public void setPublish_id(int publish_id) {
	    this.publish_id = publish_id;
	}



}
