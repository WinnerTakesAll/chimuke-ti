package jp.co.team_assignment.form;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

public class UserForm {
	private Integer id;
	private String name;
	@Email(message = "ログインに失敗したよ")
	private String mail_address;
	@NotNull(message = "ログインに失敗したよ")
	private String password;
	private int position_id;
	private String position_name;
	private String color_code;
	public  int admin_flg;
	private Timestamp created_date;
	private Timestamp updated_date;



	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail_address() {
		return mail_address;
	}
	public void setMail_address(String mail_address) {
		this.mail_address = mail_address;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getPosition_id() {
		return position_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}
	public String getColor_code() {
		return color_code;
	}
	public void setColor_code(String color_code) {
		this.color_code = color_code;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	public int getAdmin_flg() {
		return admin_flg;
	}
	public void setAdmin_flg(int admin_flg) {
		this.admin_flg = admin_flg;
	}

}
