package jp.co.team_assignment.form;

import java.sql.Timestamp;

import javax.validation.constraints.Max;

import org.hibernate.validator.constraints.NotBlank;

public class MessageForm {
	private int id;
	@Max(value = 200, message = "200文字以内で投稿してね")
	@NotBlank(message = "メッセージを投稿してね")
	private String text;
	private Timestamp created_date;
	private Timestamp updated_date;
	private int user_id;
	private int publish_id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	/**
	 * publish_idを取得します。
	 * @return publish_id
	 */
	public int getPublish_id() {
	    return publish_id;
	}
	/**
	 * publish_idを設定します。
	 * @param publish_id publish_id
	 */
	public void setPublish_id(int publish_id) {
	    this.publish_id = publish_id;
	}
}
