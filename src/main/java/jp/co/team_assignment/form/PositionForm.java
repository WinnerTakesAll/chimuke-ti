package jp.co.team_assignment.form;

import javax.validation.constraints.Min;

public class PositionForm {
	@Min(value = 1, message = "部署を選択してください")
	private int id;
	private String name;


	public PositionForm(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
