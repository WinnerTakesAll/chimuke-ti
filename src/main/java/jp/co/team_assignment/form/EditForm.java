package jp.co.team_assignment.form;

import java.sql.Timestamp;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class EditForm {
	private Integer id;

	@Pattern(regexp ="^(([0-9a-zA-Z!#\\$%&'\\*\\+\\-/=\\?\\^_`\\{\\}\\|~]+(\\.[0-9a-zA-Z!#\\$%&'\\*\\+\\-/=\\?\\^_`\\{\\}\\|~]+)*)|(\"[^\"]*\"))@[0-9a-zA-Z!#\\$%&'\\*\\+\\-/=\\?\\^_`\\{\\}\\|~]+(\\.[0-9a-zA-Z!#\\$%&'\\*\\+\\-/=\\?\\^_`\\{\\}\\|~]+)*$",
			message ="メールアドレスが不正やで")
	@Size(max=40)
	@NotBlank(message = "メールアドレスを入力してな")
	private String mail_address;
	private int[] personality_id;

	@NotBlank(message = "名前を入力してな")
    private String name;

	@Min(value = 1, message = "部署を選択してな")
	private int position_id;

	private int hashtag_id;
	private String color_code;
	private String icon;
	private int career_id;

	@NotBlank(message = "入社年月を入力してな")
	@Pattern(regexp = "[0-9]{6}", message = "入社年月は数字6桁で入力してな")
	private String joined_date;

	private String profile_text;
	private Timestamp created_date;
	private Timestamp updated_date;

	@AssertTrue(message="同じパスワードを入力してな")
	public boolean isValidPassword(){
		if(this.password.equals(this.confirmPassword)) {
			return true;
		}
		return false;
	}
	@AssertTrue(message="プロフィールは100文字以内で入力してな")
	public boolean isValidProfile() {
		if(this.profile_text != null && this.profile_text.length() <= 100) {
			return true;
		}
	return false;
	}

	@Size(max=30)
/*	@NotBlank(message = "パスワードを入力してね")*/
	private String password;

/*	@NotBlank(message = "確認用パスワードを入力してね")*/
	private String confirmPassword;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMail_address() {
		return mail_address;
	}
	public void setMail_address(String mail_address) {
		this.mail_address = mail_address;
	}
	public int[] getPersonality_id() {
		return personality_id;
	}
	public void setPersonality_id(int[] personality_id) {
		this.personality_id = personality_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosition_id() {
		return position_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}
	public int getHashtag_id() {
		return hashtag_id;
	}
	public void setHashtag_id(int hashtag_id) {
		this.hashtag_id = hashtag_id;
	}
	public String getColor_code() {
		return color_code;
	}
	public void setColor_code(String color_code) {
		this.color_code = color_code;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getCareer_id() {
		return career_id;
	}
	public void setCareer_id(int career_id) {
		this.career_id = career_id;
	}
	public String getJoined_date() {
		return joined_date;
	}
	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}
	public String getProfile_text() {
		return profile_text;
	}
	public void setProfile_text(String profile_text) {
		this.profile_text = profile_text;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}