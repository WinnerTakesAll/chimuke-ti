package jp.co.team_assignment.form;

import java.util.Date;

public class SearchForm {
	private int id;
	private String hashtag;
	private String hashtag_name;
	private String text;
	private String profile_text;
	private String publish;
	private String user_name;
	private String name;
	private String joined_date;
	private String firstDate;
	private String finalDate;
	private String firstDateTime;
	private String finalDateTime;
	private int career_id;
	private String position_name;
	private String searchUsers;
	private String searchTexts;
	private String searchHashtags;
	private Date created_date;


	public int getCareer_id() {
		return career_id;
	}
	public void setCareer_id(int career_id) {
		this.career_id = career_id;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public String getJoined_date() {
		return joined_date;
	}
	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}
	public String getJoined_date_disp() {
		String month = this.joined_date.substring(4);
		String ret = this.joined_date.substring(0, 4) + "年" + month;
		return ret;
	}
	public void setJoined_date_disp(String joined_date) {
		this.joined_date = joined_date;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getProfile_text() {
		return profile_text;
	}
	public void setProfile_text(String profile_text) {
		this.profile_text = profile_text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHashtag_name() {
		return hashtag_name;
	}
	public void setHashtag_name(String hashtag_name) {
		this.hashtag_name = hashtag_name;
	}
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}
	public String getSearchTexts() {
		return searchTexts;
	}
	public void setSearchTexts(String searchTexts) {
		this.searchTexts = searchTexts;
	}
	public String getSearchHashtags() {
		return searchHashtags;
	}
	public void setSearchHashtags(String searchHashtags) {
		this.searchHashtags = searchHashtags;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getFirstDate() {
		return firstDate;
	}
	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getFirstDateTime() {
		return firstDate + " 00:00:00";
	}
	public void setFirstDateTime(String firstDateTime) {
		this.firstDateTime = firstDateTime;
	}
	public String getFinalDateTime() {
		return finalDate + " 23:59:59";
	}
	public void setFinalDateTime(String finalDateTime) {
		this.finalDateTime = finalDateTime;
	}
	public String getSearchUsers() {
		return searchUsers;
	}
	public void setSearchUsers(String searchUsers) {
		this.searchUsers = searchUsers;
	}
}
