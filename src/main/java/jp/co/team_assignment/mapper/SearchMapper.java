package jp.co.team_assignment.mapper;

import java.util.List;

import jp.co.team_assignment.entity.Search;
import jp.co.team_assignment.form.SearchForm;

public interface SearchMapper {
	List<Search> searchTexts(SearchForm form);
	List<Search> searchTextsDate(SearchForm form);
	List<Search> searchHashtagsDate(SearchForm form);
	List<Search> searchHashtags(SearchForm form);
	List<Search> searchUsers(SearchForm form);
	List<Search> showAll(SearchForm form);

}
