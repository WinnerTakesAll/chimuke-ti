package jp.co.team_assignment.mapper;

import java.util.List;

import jp.co.team_assignment.entity.Message;

public interface MessageMapper {
	void textRegister(Message messageForm);
	int selectId();
	List<Message> hashtagCheck(String hashtag_name);
	Message allHashtagCheck(String hashtag_name);
	void hashtagRegister(String hashtag_name);
	int selectHashtag(String hashtag_name);
	void publishHashtagRegister(int publish_id , int hashtag_id);
	List<Message> getAlltexts(Message messageForm);
	void deleteMessage(int publish_id);


}
