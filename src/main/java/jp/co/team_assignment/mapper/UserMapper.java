package jp.co.team_assignment.mapper;

import java.util.List;

import jp.co.team_assignment.dto.UserDto;
import jp.co.team_assignment.entity.Personality;
import jp.co.team_assignment.entity.Position;
import jp.co.team_assignment.entity.User;
import jp.co.team_assignment.form.SignUpForm;

public interface UserMapper {
	int signUp(SignUpForm form);
	int selectId();
	void userPersonalityRegister(int user_id , int personality_id);
	List<Position> getPositions();
	List<Personality> getPersonalities();
	User getLoginUser(int id);
	int update(UserDto userDto);
	User login(User loginForm);
	User getUsers(String mail_address, String password);
	User getUserIcon(byte[] icon);
	User getOthers(int id);
	/*List<Personality> getLoginUserPersonalities(User loginForm);*/
}
