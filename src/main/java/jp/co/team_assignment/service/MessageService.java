package jp.co.team_assignment.service;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.team_assignment.dto.MessageDto;
import jp.co.team_assignment.entity.Message;
import jp.co.team_assignment.mapper.MessageMapper;

@Service
public class MessageService {

	@Autowired
	private MessageMapper messageMapper;

	//投稿登録
	public int textRegister(Message messageForm){
		messageMapper.textRegister(messageForm);
		int publish_id = messageMapper.selectId();
		return publish_id;
	}

	//既存ハッシュタグチェック
	public int hashtagCheck(String hashtag_name){
		List<Message> hashtag_check = new ArrayList<Message>();
		hashtag_check = messageMapper.hashtagCheck(hashtag_name);
		if (hashtag_check.isEmpty() == true) {
			return 0;
		} else {
			return 1;
		}
	}

	//ハッシュタグ登録
	public void hashtagRegister(String hashtag_name){
		messageMapper.hashtagRegister(hashtag_name);
		return ;
	}

	//ハッシュタグ参照
	public int selectHashtag(String hashtag_name){
		int hashtag_id ;
		Message hashtag_check = new Message();
		hashtag_check = messageMapper.allHashtagCheck(hashtag_name);
		hashtag_id = hashtag_check.getId();
		return hashtag_id;
	}

	//ハッシュタグと投稿の紐付け
	public void publishHashtagRegister(int publish_id , int hashtag_id ){
		messageMapper.publishHashtagRegister(publish_id , hashtag_id );
	}

	//投稿一覧表示準備
	public List<MessageDto> getAlltexts(Message messageForm){
		List<Message> text = messageMapper.getAlltexts(messageForm);
		List<MessageDto> textList = convertToDto(text);
		return textList;
	}

	//投稿一覧表示
	private List<MessageDto> convertToDto(List<Message> message) {
        List<MessageDto> textList = new LinkedList<MessageDto>();
        for (Message entity : message) {
        	MessageDto allMessageDto = new MessageDto();
            BeanUtils.copyProperties(entity, allMessageDto);
            textList.add(allMessageDto);
        }
        return textList;
    }

	//投稿削除
    public void deleteMessage(int publish_id) {
        messageMapper.deleteMessage(publish_id);
        return ;
    }



}
