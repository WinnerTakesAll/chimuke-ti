package jp.co.team_assignment.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.team_assignment.dto.SearchDto;
import jp.co.team_assignment.entity.Search;
import jp.co.team_assignment.form.SearchForm;
import jp.co.team_assignment.mapper.SearchMapper;

@Service
public class SearchService {

	@Autowired
	private SearchMapper searchMapper;

	public List<SearchDto> searchTexts(SearchForm form) {
		//ifで分岐 日付ありなし
		String firstDate = form.getFirstDate();
		String finalDate = form.getFinalDate();

		if(firstDate == null || StringUtils.isBlank(firstDate) == true
				&& finalDate == null || StringUtils.isBlank(finalDate) == true) {
			List<Search> text = searchMapper.searchTexts(form);
			List<SearchDto> textList = convertToSearchDto(text);
			return textList;
		} else {
			List<Search> textDate = searchMapper.searchTextsDate(form);
			List<SearchDto> textList = convertToSearchDto(textDate);
			return textList;
		}
	}
	private List<SearchDto> convertToSearchDto(List<Search> search) {
        List<SearchDto> textList = new LinkedList<SearchDto>();
        for (Search entity : search) {
        	SearchDto searchText = new SearchDto();
            BeanUtils.copyProperties(entity, searchText);
            textList.add(searchText);
        }
        return textList;
    }

	public List<SearchDto> searchHashtags(SearchForm form) {
		String firstDate = form.getFirstDate();
		String finalDate = form.getFinalDate();
		String hashtag = form.getHashtag();
		if(hashtag == null || StringUtils.isBlank(hashtag) == true) {
			List<Search> textAll = searchMapper.showAll(form);
			List<SearchDto> hashtagListAll = convertToHashtagDto(textAll);
		/*System.out.println(hashtag);*/
		return hashtagListAll;
		}

		if(firstDate == null || StringUtils.isBlank(firstDate) == true
				&& finalDate == null || StringUtils.isBlank(finalDate)) {
			List<Search> text = searchMapper.searchHashtags(form);
			List<SearchDto> hashtagList = convertToHashtagDto(text);
			return hashtagList;
		} else {
			List<Search> HashtagDate = searchMapper.searchHashtagsDate(form);
			List<SearchDto> hashtagList = convertToHashtagDto(HashtagDate);
			return hashtagList;
		}
	}

	private List<SearchDto> convertToHashtagDto(List<Search> search) {
        List<SearchDto> hashtagList = new LinkedList<SearchDto>();
        for (Search entity : search) {
        	SearchDto searchHashtag = new SearchDto();
            BeanUtils.copyProperties(entity, searchHashtag);
            hashtagList.add(searchHashtag);
        }
        return hashtagList;
    }

	public List<SearchDto> searchUsers(SearchForm form) {
		List<Search> user = searchMapper.searchUsers(form);
		List<SearchDto> userList = convertToUserDto(user);
		return userList;
	}
	private List<SearchDto> convertToUserDto(List<Search> search) {
		List<SearchDto> userList = new LinkedList<SearchDto>();
		for(Search entity : search) {
			SearchDto searchUser = new SearchDto();
			BeanUtils.copyProperties(entity, searchUser);
			userList.add(searchUser);
        }
        return userList;
	}
}
