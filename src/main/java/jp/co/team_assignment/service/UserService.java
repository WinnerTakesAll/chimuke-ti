package jp.co.team_assignment.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.team_assignment.dto.PersonalityDto;
import jp.co.team_assignment.dto.PositionDto;
import jp.co.team_assignment.dto.UserDto;
import jp.co.team_assignment.entity.Personality;
import jp.co.team_assignment.entity.Position;
import jp.co.team_assignment.entity.User;
import jp.co.team_assignment.form.SignUpForm;
import jp.co.team_assignment.mapper.UserMapper;

@Service
public class UserService {


	@Autowired
	private UserMapper userMapper;

	public int signUp(SignUpForm form) {
		 userMapper.signUp(form);
		 int user_id = userMapper.selectId();
		 return user_id;
	}

	public void userPersonalityRegister(int user_id , int personality_id){
		userMapper.userPersonalityRegister(user_id , personality_id);
	}

	public List<PositionDto> getPositions() {
		List<Position> positionList = userMapper.getPositions();
		List<PositionDto> resultList = convertToDto(positionList);
		return resultList;
	}
	private List<PositionDto> convertToDto(List<Position> positionList) {
	    List<PositionDto> resultList = new LinkedList<PositionDto>();
	    for (Position entity : positionList) {
	    	PositionDto dto = new PositionDto();
	        BeanUtils.copyProperties(entity, dto);
	        resultList.add(dto);
	    }
	    return resultList;
	}
	public List<PersonalityDto> getPersonalities() {
		List<Personality> personalityList = userMapper.getPersonalities();
		List<PersonalityDto> resultList = convertToPerDto(personalityList);
		return resultList;
	}
	private List<PersonalityDto> convertToPerDto(List<Personality> personalityList) {
	    List<PersonalityDto> resultList = new LinkedList<PersonalityDto>();
	    for (Personality entity : personalityList) {
	    	PersonalityDto dto = new PersonalityDto();
	        BeanUtils.copyProperties(entity, dto);
	        resultList.add(dto);
	    }
	    return resultList;
	}

/*	public List<PersonalityDto> getUserPersonalities(User loginForm) {
		List<Personality> personalitiesList = userMapper.getLoginUserPersonalities(loginForm);
		List<PersonalityDto> resultList = convertToPersonalitiesDto(personalitiesList);
		return resultList;
	}

	private List<PersonalityDto> convertToPersonalitiesDto(List<Personality> personalitiesList){
		List<PersonalityDto> resultList = new LinkedList<PersonalityDto>();
		for (Personality entity : personalitiesList) {
			PersonalityDto dto = new PersonalityDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}*/

	public UserDto getUsers(String mail_address, String password) {
		UserDto dto = new UserDto();
		User userList = userMapper.getUsers(mail_address, password);
		BeanUtils.copyProperties(userList, dto);
		userMapper.getUsers(mail_address, password);
		return dto;
	}

	public UserDto getLoginUser(int id) {
		UserDto dto = new UserDto();
		User loginUser = userMapper.getLoginUser(id);
		BeanUtils.copyProperties(loginUser, dto);
		userMapper.getLoginUser(id);
		return dto;
	}

	public UserDto getUserIcon(byte[] icon) throws IOException, SQLException {
		UserDto dto = new UserDto();
		User loginUser = userMapper.getUserIcon(icon);
		BeanUtils.copyProperties(loginUser, dto);
		userMapper.getUserIcon(icon);
		return dto;
	}
/*		if(binaryStream != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			StreamUtil.copy(binaryStream, baos);
			ret = baos.toByteArray();
		}
		return ret;*/

	public int update(UserDto dto) {
		int count = userMapper.update(dto);
		return count;
	}

	 public User login(User loginForm){
		User loginUser = userMapper.login(loginForm);
		return loginUser;
	}

	 public UserDto getOthers(int id) {
		 UserDto dto = new UserDto();
		 User otherUsers = userMapper.getOthers(id);
		 /*String text = userMapper.getOthers(id).getText();*/
		 /*if(otherUsers != null) {*/
			 BeanUtils.copyProperties(otherUsers, dto);
			userMapper.getOthers(id);
	return dto;
	}

}
