package jp.co.team_assignment.entity;

import java.sql.Timestamp;


public class Message {
	private int id;
	private String text;
	private String searchText;
	private String hashtags;
	private Timestamp created_date;
	private Timestamp updated_date;
	private int user_id;
    private String user_name;
    private String hashtag_name;
    private int publish_id;


	public String getHashtags() {
		return hashtags;
	}
	public void setHashtags(String hashtags) {
		this.hashtags = hashtags;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * textを取得します。
	 * @return text
	 */
	public String getText() {
	    return text;
	}
	/**
	 * textを設定します。
	 * @param text text
	 */
	public void setText(String text) {
	    this.text = text;
	}
	/**
	 * created_dateを取得します。
	 * @return created_date
	 */
	public Timestamp getCreated_date() {
	    return created_date;
	}
	/**
	 * created_dateを設定します。
	 * @param created_date created_date
	 */
	public void setCreated_date(Timestamp created_date) {
	    this.created_date = created_date;
	}
	/**
	 * updated_dateを取得します。
	 * @return updated_date
	 */
	public Timestamp getUpdated_date() {
	    return updated_date;
	}
	/**
	 * updated_dateを設定します。
	 * @param updated_date updated_date
	 */
	public void setUpdated_date(Timestamp updated_date) {
	    this.updated_date = updated_date;
	}
	/**
	 * user_idを取得します。
	 * @return user_id
	 */
	public int getUser_id() {
	    return user_id;
	}
	/**
	 * user_idを設定します。
	 * @param user_id user_id
	 */
	public void setUser_id(int user_id) {
	    this.user_id = user_id;
	}
	/**
	 * user_nameを取得します。
	 * @return user_name
	 */
	public String getUser_name() {
	    return user_name;
	}
	/**
	 * user_nameを設定します。
	 * @param user_name user_name
	 */
	public void setUser_name(String user_name) {
	    this.user_name = user_name;
	}
	/**
	 * hashtag_nameを取得します。
	 * @return hashtag_name
	 */
	public String getHashtag_name() {
	    return hashtag_name;
	}
	/**
	 * hashtag_nameを設定します。
	 * @param hashtag_name hashtag_name
	 */
	public void setHashtag_name(String hashtag_name) {
	    this.hashtag_name = hashtag_name;
	}
	/**
	 * publish_idを取得します。
	 * @return publish_id
	 */
	public int getPublish_id() {
	    return publish_id;
	}
	/**
	 * publish_idを設定します。
	 * @param publish_id publish_id
	 */
	public void setPublish_id(int publish_id) {
	    this.publish_id = publish_id;
	}



}
