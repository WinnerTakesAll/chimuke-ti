package jp.co.team_assignment.entity;

import java.util.List;

public class Personality {
	private int id;
	private String name;
	private List<Personality> personalities_name;

	public List<Personality> getPersonalities_name() {
		return personalities_name;
	}
	public void setPersonalities_name(List<Personality> personalities_name) {
		this.personalities_name = personalities_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
