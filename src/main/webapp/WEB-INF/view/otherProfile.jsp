<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
<title>プロフィール</title>
<script type="text/javascript" src="main.js"></script>
</head>
<body>
	<header>
		<a href="../../profile/">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C"  id="header-logo" >
		</a>
		<div class="right-menu">
		<a href="../../profile/"><i class="fas fa-home" >プロフィール</i></a>
		<a href="../../search/"><i class="fas fa-search">検索</i></a>
		<a href="../../update/${loginUser.id}/"><i class="fas fa-edit">編集</i></a>
		<c:if test="${loginUser.admin_flg != 0}">
		<a href="../../signUp/"><i class="fas fa-user-plus">新規登録</i></a></c:if>
		<a href="../../login/"><i class="fas fa-sign-out-alt">ログアウト</i></a>		</div>
		<div class="header-bottom1"></div>
		<div class="header-bottom2"></div>
		<div class="header-bottom3"></div>
		<div class="header-bottom4"></div>
		<div class="header-bottom5"></div>
	</header>


<span id="left-area">
<h2>${otherUsers.name}さんの<br>
　　プロフィール <img src="./profile/?id=${search.id}"></h2>
   	<span id="profile-box2">
	<form:form modelAttribute="SearchForm" method="get" class="otherPlofile-search">
		<div><form:errors path="*"  /></div><br>
		<form:input path="id" value="${search.id}" name="others" type="hidden"/>
<div class="user-name">
		    ${otherUsers.name} <img src="./profile/?id=${loginUser.id}">
		</div>
   		 プロフィール: <br>${otherUsers.profile_text}<br><br>
   		 部署: ${otherUsers.position_name}<br>
   		 ${otherUsers.joined_date_disp}月
   		 <c:if test="${otherUsers.career_id == 0}">
		    新卒入社
		 </c:if>
		 <c:if test="${otherUsers.career_id == 1}">
		    中途入社
		</c:if>
		<%-- ${otherUsers.personalities_name} --%>
	</form:form>
	</span>
</span>

<div class="right-area">
	<h2>投稿一覧</h2>

		<c:forEach items="${textLists}" var="textList">
			<c:if test="${otherUsers.text != null}">
				<p>本文: <c:out value="${textList.text}"></c:out></p>
					<p><c:if test="${ not empty textList.hashtag_name }">
		 		<p>#<c:out value="${textList.hashtag_name}"></c:out></p></c:if>
				<p>by<c:out value="${otherUsers.name}"></c:out></p>
				<p>日時:<c:out value="${textList.created_date}"></c:out></p><br>
			</c:if>
		</c:forEach>
			 	<h4>${message}</h4>

</div>
<!--
<a href="/team_assignment/profile/">自分のプロフィールに戻る</a> -->
</body>
</html>