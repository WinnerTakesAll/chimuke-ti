<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
<title>プロフィール</title>
<script type="text/javascript" src='<c:url value="/resources/js/jquery-3.3.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/javascript.js"/>'></script>
</head>
<body>
	<header>
		<a href="../profile/">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C"  id="header-logo" >
		</a>
		<div class="right-menu">
		<a href="../search/"><i class="fas fa-search">検索</i></a>
		<a href="../update/${loginUser.id}/"><i class="fas fa-edit">編集</i></a>
		<c:if test="${loginUser.admin_flg != 0}">
		<a href="../signUp/"><i class="fas fa-user-plus">新規登録</i></a></c:if>
		<a href="../login/"><i class="fas fa-sign-out-alt">ログアウト</i></a>		</div>
		<div class="header-bottom1"></div>
		<div class="header-bottom2"></div>
		<div class="header-bottom3"></div>
		<div class="header-bottom4"></div>
		<div class="header-bottom5"></div>
	</header>




<!-- 	<main class="Main" role="main">
	  <div id="borderBlack" class="border">
	      <div class="border-inner"></div>
	      <div class="border-inner"></div>
	      <div class="border-inner"></div>
	      <div class="border-inner"></div>
	  </div>
	      <div id="wrapper"></div>
	</main> -->





<%-- 	<p id="plofile-title">${profileMessage}</p> --%>
    <span id="left-area">
    	<span id="profile-box">

    	<div class="user-name">
		    ${loginUser.name} <img src="./profile/?id=${loginUser.id}">
		</div><c:if test="${loginUser.profile_text != null}"><br>
		   プロフィール:<br>${loginUser.profile_text}<br></c:if><br>

		    部署: ${loginUser.position_name}<br>
		    ${loginUser.joined_date_disp}月
		    <%-- ${fn:substring( "${loginUser.joined_date}" , 1 , 4 ) } 年 --%>

		    <c:if test="${loginUser.career_id == 0}">
		    新卒入社
		    </c:if>
		    <c:if test="${loginUser.career_id == 1}">
		    中途入社
		    </c:if>
	    </span>
	</span>


<%-- 		<span class="popupModal1">
			<input type="radio" name="modalPop" id="pop11" />
			<label for="pop11">投稿</label>
			<input type="radio" name="modalPop" id="pop12" />
			<label for="pop12">CLOSE</label>
			<input type="radio" name="modalPop" id="pop13" />
			<label for="pop13">×</label>
			<div class="modalPopup2">
				<div class="modalPopup3">
					<h2 class="modalTitle">投稿してね</h2>
					<div class="modalMain">
						<form:form modelAttribute="messageForm" method="post">
							<c:if test="${ not empty errorMessages }">
						    	<div class="errorMessages">
							        <c:forEach items="${errorMessages}" var="message">
							        	<c:out value="${message}" />
							        </c:forEach>
						        </div>
						        <c:remove var="errorMessages" scope="session"/>
						    </c:if><br>
							<form:textarea path="text" cols="60" rows="15" placeholder="本文を入力..."/>
								<p id="text-description">本文は400字以下で入力してください</p>
								<textarea id="hashtag-form" name="hashtag_name" cols="30" rows="8" placeholder="タグを入力..."></textarea>
								<input type="hidden" name="user_id" value="${loginUser.id}">
								<input type="hidden" name="loginUser" value="${loginUser.admin_flg}">
								<p id="hashtag-description">ハッシュタグは「全角スペース」「半角スペース」「,」「、」で区切られます</p>
								<input type="submit" name="publish" value="投稿" class="publish-btn"/>
						</form:form>
					</div>
				</div>
			 </div>
		</span>
	</span> --%>


<div class="right-area">


 		<%-- <p><c:if test="${textList.hashtag_name == null}">
 		<c:out value="${textList.hashtag_name}"></c:out></c:if></p> --%>
	<form:form modelAttribute="messageForm" method="post">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
			    <c:forEach items="${errorMessages}" var="message">
			       	<c:out value="${message}" />
			    </c:forEach>
		    </div>

		    <c:remove var="errorMessages" scope="session"/>
		</c:if><br>

		<form:textarea path="text" cols="60" rows="8" maxlength="400" placeholder="本文は400字以内で書いてな"/>
		<!-- <p id="text-description">本文は400字以下で入力してください</p> -->

		<textarea id="hashtag-form" name="hashtag_name" cols="30" rows="2" maxlength="150"  placeholder="タグは「全角スペース」「半角スペース」「,」「、」で区切るで"></textarea>
		<input type="hidden" name="user_id" value="${loginUser.id}">
		<input type="hidden" name="loginUser" value="${loginUser.admin_flg}">
		<!-- <p id="hashtag-description">ハッシュタグは「全角スペース」「半角スペース」「,」「、」で区切られます</p> -->
		<input type="submit" name="publish" value="投稿するで" class="square_btn"/>

	</form:form>
	<div class="splitborder"></div>

	<div class="user-publishes">
		<c:forEach items="${textLists}" var="textList">
			 <c:if test="${textList.user_id == loginUser.id }">
				<form:form modelAttribute="messageForm" method="post">
					<input type="hidden" name="publish_id" value="${textList.id}">
					<div class="delete">
					<button type="submit" name="deleteMessage" class="publish-delete-btn" onClick="return disp();">
					<i class="fas fa-trash-alt fa-lg"></i>
					</button></div>
				</form:form>
			 </c:if>

			<p class="publish-text"><c:out value="${textList.text}"></c:out></p>
			<p class="publish-hashtag">
				<c:if test="${ not empty textList.hashtag_name }">
	 				#<c:out value="${textList.hashtag_name}"></c:out>
	 			</c:if>
	 		</p>
	 		<p class="publish-created-date">
	 			<c:out value="${textList.created_date}"></c:out>
	 		</p>


	 		<%-- <p><c:if test="${textList.hashtag_name == null}">
	 		<c:out value="${textList.hashtag_name}"></c:out></c:if></p> --%>
			<p class="publish-user">by<c:out value="${loginUser.name}"></c:out></p>
					<input type="hidden" name="loginUser_id" value="${loginUser.id}">

			<c:if test="${ empty textList.text }">
			<c:out value="${messages}" /></c:if>

		</c:forEach>

		<c:forEach items= "${messages}">
		</c:forEach>
	</div>
</div>



<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('この投稿を削除しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
// -->
</script>
</body>
</html>