<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>ユーザー登録</title>
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
    </head>
    <body>
		<header>
		<a href="../profile/">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C"  id="header-logo" >
		</a>
		<div class="right-menu">
		<a href="../profile/"><i class="fas fa-home" >プロフィール</i></a>
		<a href="../search/"><i class="fas fa-search">検索</i></a>
		<a href="../update/${loginUser.id}/"><i class="fas fa-edit">編集</i></a>
				<a href="../login/"><i class="fas fa-sign-out-alt">ログアウト</i></a>		</div>
		<div class="header-bottom1"></div>
		<div class="header-bottom2"></div>
		<div class="header-bottom3"></div>
		<div class="header-bottom4"></div>
		<div class="header-bottom5"></div>
	</header>

	<h2>${message}</h2>

       	<form:form modelAttribute="SignUpForm">
       	 	<div><form:errors path="*"  /></div><br>

       	 	<form:label path="name">名前</form:label>
       	 		<form:input path="name" /><br>
       	 	<form:label path="mail_address">メールアドレス</form:label>
       	 		<form:input path="mail_address" /><br>
       	 	<form:label path="password">パスワード</form:label>
       	 		<form:password path="password" showPassword="false"/><br>

       	 	<form:label path="confirmPassword">確認用パスワード</form:label>
       	 		<form:password path="confirmPassword" showPassword="false"/><br>

       	 	<form:label path="position_id">部署</form:label>
       	 		<form:select path="position_id" >
       	 			<form:option label="選択してな" value="0"/>
       	 			<form:options  multiple="false" itemLabel="name" itemValue="id" items="${positionList}" />
       	 		</form:select> <br>

			<form:label path="joined_date">入社年月</form:label>
				<form:input path="joined_date" />(例 2018年4月入社→201804)<br>

 			<form:label path="personality_id">パーソナリティー</form:label>
				<form:checkboxes path="personality_id" itemLabel="name" itemValue="id" items="${checkPersonalities}" multiple="true"/><br>

			<form:label path="career_id">キャリア</form:label>
				<form:radiobutton path="career_id" label="新卒入社" value="0"/>
				<form:radiobutton path="career_id" label="中途入社" value="1"/><br>

			<form:label path="color_code">テーマカラー</form:label><br>

			<form:label path="icon">アイコン</form:label><br>
			<p><form:input type="file" value="${loginUser.icon}" path="icon"/><p> <br>

			<form:label path="profile_text">プロフィール</form:label><br>
				<form:textarea path="profile_text" cols="20" rows="5" /><br>

        	<input type="submit" value="登録"/>

    	</form:form><br>

<!--     	<a href="../profile/">プロフィールに戻る</a> -->

<%--         <h2>${users.name}</h2>
        <h2>${users.mail_address}</h2>
        <h2>${users.positionList}</h2>
        <h2>${users.password}</h2>
        <h2>${users.joined_date}</h2> --%>


    </body>
</html>