<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>検索</title>
</head>
<body>
	<header>
		<a href="../profile/">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C"  id="header-logo" >
		</a>
		<div class="right-menu">
			<a href="../profile/"><i class="fas fa-home" >プロフィール</i></a>
<!-- 			<a href="../search/"><i class="fas fa-search">検索</i></a> -->
			<a href="../update/${loginUser.id}/"><i class="fas fa-edit">編集</i></a>
			<c:if test="${loginUser.admin_flg != 0}">
			<a href="../signUp/"><i class="fas fa-user-plus">新規登録</i></a></c:if>
		<a href="../login/"><i class="fas fa-sign-out-alt">ログアウト</i></a>		</div>
		<div class="header-bottom1"></div>
		<div class="header-bottom2"></div>
		<div class="header-bottom3"></div>
		<div class="header-bottom4"></div>
		<div class="header-bottom5"></div>
	</header>

	<h2>${message}</h2><br>

			<a href="../search/"><i class="fas fa-redo-alt">リセット</i></a>
<div class="none">
		<form:form modelAttribute="SearchForm" action="../search/" method="get">
		<div><form:errors path="*"  /></div>

		<div class="search-date">
			期間検索<br>
			<form:input type="date" path="firstDate" />～<form:input type="date" path="finalDate" />
			<!-- <input type="submit"  name="searchTexts" class="search-btn"/> -->
			<input type="submit"  name="searchTexts"  class="fa btn1" value="&#xf002; 検索">

		</div>




		<div class="search-text">
			<form:label path="publish">投稿検索</form:label><br>
 	        <form:input path="publish" />
	        <input type="submit"  name="searchTexts"  class="fa btn2" value="&#xf002; 検索">
<p><br>
	    </div>
<%--
	         <c:forEach items="${searchTexts}" var="search">
	         	<p>
	         	本文: <c:out value="${search.text}"><br></c:out>
	         	<c:if test="${ not empty search.hashtag_name }">
	         	<br>#</c:if><c:out value="${search.hashtag_name}"><br></c:out><br>
	         	by<c:out value="${search.name}"><br></c:out><br>
	         	 <c:out value="${search.created_date}"></c:out><br>
	         	</p>
	         </c:forEach> --%>

		<div class="search-hashtag">
	        <form:label path="hashtag">ハッシュタグ検索</form:label><br>
	        <form:input path="hashtag" />
	        <input type="submit"  name="searchTags"  class="fa btn3" value="&#xf002; 検索">
			</div>

<div class="user-publishes">
	         <c:forEach items="${searchTexts}" var="search">
	         	<p class="publish-text"><c:out value="${search.text}"></c:out></p>
	         	<p class="publish-hashtag"><c:if test="${ not empty search.hashtag_name }">
	         	#<c:out value="${search.hashtag_name}"></c:out>
	         	</c:if>
	         	</p>

	         	<p class="publish-created-date">
	         	<c:out value="${search.created_date}"></c:out>
	         	</p>

	         	<p class="publish-user">
	         	by<c:out value="${search.name}"><br></c:out></p>

	         </c:forEach>
</div>


<div class="user-publishes">
	         <c:forEach items="${searchHashtags}" var="search">
	         	<p class="publish-text"><c:out value="${search.text}"></c:out></p>
	         	<p class="publish-hashtag"><c:if test="${ not empty search.hashtag_name }">
	         	#<c:out value="${search.hashtag_name}"></c:out>
	         	</c:if>
	         	</p>

	         	<p class="publish-created-date">
	         	<c:out value="${search.created_date}"></c:out>
	         	</p>

	         	<p class="publish-user">
	         	by<c:out value="${search.name}"><br></c:out></p>

	         </c:forEach>
</div>




			<div class="input-search-user">
	        <form:label path="user_name">ユーザー名検索</form:label><br>
	        <form:input path="user_name" />
	        <input type="submit"  name="searchUsers"  class="fa btn4" value="&#xf002; 検索"></div>

	    </form:form>


	    <form:form modelAttribute="UserForm" action="../search/" method="get">

			<div class="result-search-user" >
	    	<c:forEach items="${searchUsers}" var="search">
            	 <a href="../profile/${search.id}/">${search.name}</a>
<%--              <input type="hidden" name="others" value="${others.id}"> --%>
           	  <br>
                <%-- <c:out value="${search}"></c:out> --%>
        	</c:forEach>
        	</div>
        </form:form>
        <br><br>

<!--      <a href="../profile/">プロフィールに戻る</a> -->
<br>

</body>
</html>