<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
<title>ログイン</title>
<script type="text/javascript" src='<c:url value="/resources/js/jquery-3.3.1.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/javascript.js"/>'></script>

</head>
<body>
<div class="main-container">
	<div class="logo-area">
		<div class="logo">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C" width="120%" height="100%" >
		</div>
	</div>
	<div class="main-area">
		<p id=login-message>社内の人脈、広げへん？</p>
		<%-- <h1 id="sign-in" >${message}</h1> --%>
		<form:form modelAttribute="userForm" >
			<div><form:errors path="*"  /></div><br>
		<%--  action="../profile/" --%>
				<form:input path="mail_address" placeholder="Email Address" class="required"/><br/><br/>
				<form:input type="password" path="password" showPassword="false" placeholder="Password" class="required"/>
		        <input type="submit" id="sign-in" value="Sign In"  />

	    </form:form>
	</div>
</div>

</body>
</html>