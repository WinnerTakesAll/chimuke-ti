<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>ユーザー編集</title>
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
    </head>
    <body>
	<header>
		<a href="../../profile/">
			<img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-0/s180x540/18485929_102296287022938_2353850535678539105_n.png?_nc_cat=0&oh=eab1aaef25b6ccc9debf7b6697912dc8&oe=5BE2B29C"  id="header-logo" >
		</a>
		<div class="right-menu">
			<a href="../../profile/"><i class="fas fa-home" >プロフィール</i></a>
			<a href="../../search/"><i class="fas fa-search">検索</i></a>
			<c:if test="${loginUser.admin_flg != 0}">
			<a href="../../signUp/"><i class="fas fa-user-plus">新規登録</i></a></c:if>
		<a href="../../login/"><i class="fas fa-sign-out-alt">ログアウト</i></a>		</div>
		<div class="header-bottom1"></div>
		<div class="header-bottom2"></div>
		<div class="header-bottom3"></div>
		<div class="header-bottom4"></div>
		<div class="header-bottom5"></div>
	</header>
	    	<h2>${message}</h2>

       	<form:form modelAttribute="EditForm">
       	 	<div><form:errors path="*"  /></div><br>
       	 	<form:input path="id" value="${loginUser.id}" type="hidden"/>

       	 	<p><form:label path="name">名前</form:label>
       	 		<form:input path="name" value="${loginUser.name}" /><br></p>
       	 	<p><form:label path="mail_address">メールアドレス</form:label>
       	 		<form:input path="mail_address" value="${loginUser.mail_address}"/><br></p>
       	 	<p><form:label path="password">パスワード</form:label>
       	 		<form:password path="password" showPassword="false"/></p>

       	 		<p><form:label path="confirmPassword">確認用パスワード</form:label>
       	 	<form:password path="confirmPassword" showPassword="false"/></p>

       	 	<p><form:label path="position_id">部署</form:label>
       	 		<form:select path="position_id" >
       	 			<form:option label="選択してな" value="0"/>
<%--        	 				<c:forEach items="${positionList}" var="position">
       	 					<c:if test="${position.id == user.position_id}">
       	 			-- 			<form:options multiple="false" itemLabel="name" itemValue="id" items="${positionList}" selected="selected"/>
       	 					</c:if>
       	 					<c:if test="${position.id != user.position_id}"> --%>
       	 						<form:options multiple="false" itemLabel="name" itemValue="id" items="${positionList}"/>
       	 				<%-- 	</c:if>
       	 				</c:forEach> --%>
       	 		</form:select> <br></p>

			<p><form:label path="joined_date">入社年月</form:label>
				<form:input path="joined_date" value="${loginUser.joined_date}"/>(例 2018年4月入社→201804)<br></p>

 			<p><form:label path="personality_id">パーソナリティー</form:label>
				 <form:checkboxes path="personality_id" items="${checkPersonalities}" itemLabel="name" itemValue="id" multiple="true" selected="selected"/><br></p>

				<c:if test="${loginUser.career_id == 0}">
			<p><form:label path="career_id">キャリア</form:label>
				<form:radiobutton path="career_id" label="新卒入社" value="0" selected="selected"/>
				<form:radiobutton path="career_id" label="中途入社" value="1"/><br></p>
				</c:if>

				<c:if test="${loginUser.career_id != 0}">
			<p><form:label path="career_id">キャリア</form:label>
				<form:radiobutton path="career_id" label="新卒入社" value="0"/>
				<form:radiobutton path="career_id" label="中途入社" value="1" selected="selected"/><br></p>
				</c:if>

			<p><form:label path="color_code">テーマカラー</form:label><br></p>

			<p><form:label path="icon">アイコン</form:label><br>
			<form:input type="file" path="icon" src="${loginUser.icon}" /></p>
			<br>

			<p><form:label path="profile_text">プロフィール</form:label><br>
				<form:textarea path="profile_text" cols="20" rows="5" value="${loginUser.profile_text}"/><br></p>

        	<input type="submit" value="更新"/>

    	</form:form><br>

    	<!-- <a href="/team_assignment/profile/">プロフィールに戻る</a>
 -->
 </body>
</html>
