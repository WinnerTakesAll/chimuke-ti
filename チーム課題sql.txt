create table users(
	id          	INTEGER       		AUTO_INCREMENT PRIMARY KEY,
	mail_address		VARCHAR(40)		NOT NULL,
	personality_id	INTEGER,
	password		VARCHAR(255)		NOT NULL,
	name			VARCHAR(20)			NOT NULL,
	position_id		INTEGER				NOT NULL,
	hashtag_id		INTEGER,
	color_code		VARCHAR(6),
	icon 			BLOB,
	career_id		TINYINT,
	joied_date		VARCHAR(6)			NOT NULL,
	profile_text	VARCHAR(100),
	created_date 	TIMESTAMP      		NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date 	TIMESTAMP       	NULL DEFAULT CURRENT_TIMESTAMP
);

create table positions(
	id          	INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(10)		UNIQUE NOT NULL
	admin_flg		TINYINT         NOT NULL
);

create table publishes(
	id    		    INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	text			VARCHAR(400)		NOT NULL,
	created_date 	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_date 	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id		    INTEGER			NOT NULL
);

create table comments(
	id    		    INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	publish_id		INTEGER			NOT NULL,
	text			VARCHER(200)	NOT NULL,
	created_date 	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_date	TIMESTAMP   	NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id		    INTEGER			NOT NULL
);

create table hashtags(
	id	 			INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR			NOT NULL
);

create table personalities(
	id	 			INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(15)		NOT NULL
);

create table favorites (
	id	 			INTEGER       	AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(10)			NOT NULL
);






